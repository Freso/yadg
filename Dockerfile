FROM python:2.7-slim

WORKDIR /usr/src/app

RUN groupadd -g 999 yadg && \
    useradd -r -u 999 -g yadg yadg

COPY requirements.txt ./
COPY requirements.deploy.txt ./
RUN apt-get update && apt-get install -y gosu build-essential libxml2-dev libxslt1-dev zlib1g-dev && rm -rf /var/lib/apt/lists/* && \
    pip install --no-cache-dir -r requirements.txt -r requirements.deploy.txt && \
    apt-get purge -y build-essential && apt-get autoremove -y && \
    chown yadg:yadg -R .

COPY --chown=yadg:yadg . .

ARG commit_ref=$$COMMIT_REF$$

RUN find -type f -print0 | xargs -0 sed -i "s/\\\$\\\$COMMIT_REF\\\$\\\$/${commit_ref}/g"

ENTRYPOINT ["/usr/src/app/docker/docker-entrypoint.sh"]

CMD [ "gunicorn", "--config=gunicorn.py.ini", "wsgi:application" ]