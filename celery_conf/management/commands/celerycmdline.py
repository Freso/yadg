#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from celery_conf.routers import Router


class Command(BaseCommand):
    help = 'Returns a list of shell commands needed to start the necessary Celery worker nodes and setting the ' \
           'defined rate limits.'

    def add_arguments(self, parser):
        parser.add_argument('mode', help='the mode for celery multi')
        parser.add_argument('-a', '--app', help='the celery app to execute', default='celery_conf:app')
        parser.add_argument('-n', '--node-ids',
                            help='a list of node ids to print the commands for (if emtpy all will be printed)',
                            nargs='*', type=int)
        parser.add_argument('--hostname', help='the hostname to use', default='yadg')
        parser.add_argument('-l', '--logfile',
                            help='the path to the worker logfile(s) ("%N" will be replaced by worker id)')
        parser.add_argument('--log-level', help='the desired log level for celery', default='info')
        parser.add_argument('-p', '--pid-file',
                            help='the path to the worker pid file(s) ("%N" will be replaced by worker id)')

    def handle(self, *args, **options):
        mode = options['mode'].lower()
        if mode not in ['nodes', 'start', 'rate_limit']:
            raise CommandError('invalid celery multi mode: %s' % mode)

        celery_app = options['app']
        hostname = options['hostname']
        pid_file_path = options.get('logfile')
        log_file_path = options.get('pid_file')
        log_level = options['log_level']
        node_ids = options.get('node_ids')

        queue_factory = settings.CELERY_QUEUE_FACTORY
        task_name = Router.task_name

        start_commands = []
        rate_limit_commands = []
        available_node_ids = []
        queue_rate_limits = queue_factory.get_queue_rate_limits()

        i = 0
        for queue in queue_factory.get_queues():
            i += 1
            if node_ids is not None and i not in node_ids:
                continue
            available_node_ids.append(i)
            rate_limit, concurrency = queue_rate_limits.get(queue.name, (None, None))

            start_command = 'celery worker -n celery{id:d}@{host} -A {app} -l {log_level} -Q {queue_name}'.format(
                id=i, app=celery_app, host=hostname, queue_name=queue.name, log_level=log_level)
            if concurrency is not None:
                start_command += ' -c {:d}'.format(concurrency)
            if pid_file_path is not None:
                start_command += ' --pidfile={}'.format(pid_file_path.replace('%N', i))
            if log_file_path is not None:
                start_command += ' --logfile={}'.format(log_file_path.replace('%N', i))
            start_commands.append(start_command)

            if rate_limit is not None:
                rate_limit_commands.append(
                    'celery -A {app} control -t 3 -d celery{id:d}@{host} rate_limit {task_name} {rate_limit}'.format(
                        app=celery_app, id=i, host=hostname, task_name=task_name, rate_limit=rate_limit))

        lines_to_print = []
        if mode == 'nodes':
            lines_to_print = map(str, available_node_ids)
        elif mode == 'start':
            lines_to_print = start_commands
        elif mode == 'rate_limit':
            lines_to_print = rate_limit_commands

        print('\n'.join(lines_to_print))
