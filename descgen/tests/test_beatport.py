#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import TestCase
from ..scraper import beatport
from ..result import NotFoundResult


class BeatportTest(TestCase):

    def test_remix_track_artist(self):
        expected = {
            u'artists': [
                {u'name': u'Homemade Weapons', u'types': [u'main']},
                {u'name': u'Donato Dozzy', u'types': [u'remixer']},
                {u'name': u'Tommy Four Seven', u'types': [u'remixer']}
            ],
            u'title': u'Gravity (Remixed)',
            u'genres': [u'Drum & Bass'],
            u'releaseEvents': u'2019-11-29',
            u'labelIds': [{u'catalogueNrs': [u'SMDELP04RX'], u'label': u'Samurai Music'}],
            u'url': u'https://www.beatport.com/release/gravity-remixed/2771002',
            u'discs': [
                [
                    [u'Homemade Weapons', u'Svalsat [Donato Dozzy Remix]', 414],
                    [u'Homemade Weapons', u'Red Tide [Tommy Four Seven Remix]', 261],
                ]
            ]
        }

        scraper = beatport.ReleaseScraper.from_string('https://www.beatport.com/release/gravity-remixed/2771002')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_various_artists(self):
        expected = {
            u'artists': [{u'isVarious': True, u'name': None, u'types': [u'main']}],
            u'releaseEvents': u'2018-09-28',
            u'labelIds': [{u'catalogueNrs': [u'SMDEC2'], u'label': u'Samurai Music'}],
            u'genres': [u'Breaks / Breakbeat / UK Bass', u'Drum & Bass'],
            u'title': u'Samurai Music Decade (Phase 2)',
            u'url': u'https://www.beatport.com/release/samurai-music-decade-phase-2/2385385',
            u'discs': [
                [
                    [u'Ancestral Voices', u'Hypersigil', 372],
                    [u'ASC', u'Area Check', 367],
                    [u'Lemna', u'Rahu', 399],
                    [u'Sam Kdc', u'Locus', 306],
                    [u'The Untouchables', u'Zaku', 316],
                    [u'Shiken Hanzo', u'Khans of Takir', 368],
                    [u'Last Life', u'The Worst Awakening', 378],
                    [u'Estereo', u'LV426', 288],
                    [u'Calibre', u'Snoopers Dub', 384],
                    [u'Antagonist', u'Below', 449],
                    [[u'Torn', u'Homemade Weapons'], u'Spectre', 372],
                    [u'Artilect', u'System of Fear', 364],
                    [u'Torn', u'Unjustified Expectations', 477],
                    [[u'Homemade Weapons', u'Last Life'], u'Lahar', 401],
                ]
            ]
        }

        scraper = beatport.ReleaseScraper.from_string('https://www.beatport.com/release/samurai-music-decade-phase-2/2385385')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_404(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        s = beatport.ReleaseScraper.from_string('https://www.beatport.com/release/blubb/123')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_search_scraper(self):
        s = beatport.SearchScraper('love')
        r = s.get_result()

        self.assertGreater(len(r.get_items()), 0)
