#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import lxml
from .base import Scraper, GetReleaseResultMixin, GetListResultMixin, ExceptionMixin, RequestMixin, UtilityMixin, StandardFactory
from .base import SearchScraper as SearchScraperBase

READABLE_NAME = 'Beatport'
SCRAPER_URL = 'https://www.beatport.com/'


class GenericResultScraper(object):
    def _get_data(self, index):
        if index < len(self.data):
            return self.data[index].get('state', {}).get('data', {})

    def process_initial_data(self, initial_data):
        try:
            page = lxml.html.document_fromstring(initial_data)
            next_data = page.cssselect('script[id="__NEXT_DATA__"]')
            data = json.loads(next_data[0].text_content())
            return data.get('props').get('pageProps').get('dehydratedState').get('queries')
        except (ValueError, IndexError):
            self.raise_exception(u'Could not parse "__NEXT_DATA__"')


class ReleaseScraper(GenericResultScraper, GetReleaseResultMixin, Scraper, RequestMixin, ExceptionMixin, UtilityMixin):
    _RELEASE_DATA = 0
    _TRACKS_DATA = 1

    string_regex = '^https?://(?:(?:www|pro|classic)\.)?beatport\.com/release(?:/([^/]+))?(?:/(\d+))$'

    def __init__(self, id, release_name=''):
        super(ReleaseScraper, self).__init__()
        self.id = id
        self.release_name = release_name

    def get_instance_info(self):
        return u'id=%d release_name="%s"' % (self.id, self.release_name)

    @staticmethod
    def _get_args_from_match(match):
        if not match.group(1):
            return int(match.group(2)),
        return int(match.group(2)), match.group(1)

    def get_url(self):
        return 'https://www.beatport.com/release/%s/%d' % (self.release_name, self.id)

    def get_headers(self):
        headers = super(ReleaseScraper, self).get_headers()
        headers['Accept-Language'] = 'en,en-US'
        return headers

    def add_release_event(self):
        release_date = self._get_data(self._RELEASE_DATA).get('new_release_date')
        if release_date:
            release_event = self.result.create_release_event()
            release_event.set_date(release_date)
            self.result.append_release_event(release_event)

    def add_label_ids(self):
        release_label_name = self._get_data(self._RELEASE_DATA).get('label', {}).get('name')
        release_catalog_nr = self._get_data(self._RELEASE_DATA).get('catalog_number')
        if release_label_name:
            label_id = self.result.create_label_id()
            label_id.set_label(release_label_name)
            if release_catalog_nr:
                label_id.append_catalogue_nr(release_catalog_nr)
            self.result.append_label_id(label_id)

    def add_release_title(self):
        release_title = self._get_data(self._RELEASE_DATA).get('name')
        if release_title:
            self.result.set_title(release_title)

    def add_release_artists(self):
        release_artists = self._get_data(self._RELEASE_DATA).get('artists', [])
        if len(release_artists) > 3:
            artist = self.result.create_artist()
            artist.set_various(True)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)
        else:
            for release_artist in release_artists:
                artist = self.result.create_artist()
                artist.set_name(release_artist.get('name'))
                artist.append_type(self.result.ArtistTypes.MAIN)
                self.result.append_release_artist(artist)
        release_remix_artists = self._get_data(self._RELEASE_DATA).get('remixers', [])
        if release_remix_artists:
            for release_remix_artist in release_remix_artists:
                artist = self.result.create_artist()
                artist.set_name(release_remix_artist.get('name'))
                artist.append_type(self.result.ArtistTypes.REMIXER)
                self.result.append_release_artist(artist)

    def get_track_genre(self, track_container):
        track_genre = track_container.get('genre', {}).get('name')
        return track_genre if track_genre else None

    def get_track_artists(self, track_container):
        artists = []
        track_artists = track_container.get('artists', [])
        if track_artists:
            for track_artist in track_artists:
                artist = self.result.create_artist()
                artist.set_name(track_artist.get('name'))
                artist.append_type(self.result.ArtistTypes.MAIN)
                artists.append(artist)
        return artists

    def get_track_title(self, track_container):
        track_title = track_container.get('name')
        track_remix_title = track_container.get('mix_name')
        if track_title and track_remix_title:
            if track_remix_title.lower() == 'original mix':
                return track_title
            return u'%s [%s]' % (track_title, track_remix_title)
        elif not track_remix_title:
            return track_title
        return None

    def get_track_length(self, track_container):
        track_length = track_container.get('length_ms', 0)
        return track_length / 1000 if track_length else None

    def get_track_number(self, track_container):
        return track_container.get('_track_number')

    def get_disc_containers(self):
        return {1: self._get_data(self._TRACKS_DATA).get('results', [])[::-1]}

    def get_track_containers(self, disc_container):
        return [dict(track, _track_number=str(index)) for (index, track) in enumerate(disc_container, start=1)]


class SearchScraper(GenericResultScraper, GetListResultMixin, SearchScraperBase, RequestMixin, ExceptionMixin, UtilityMixin):
    url = 'https://www.beatport.com/search/releases'

    def get_params(self):
        return {'q': self.search_term}

    def get_url(self):
        return self.url

    def get_release_containers(self):
        return self._get_data(0).get('data')[:25]

    def get_release_name(self, release_container):
        components = []
        release_artists = release_container.get('artists', [])
        if release_artists:
            for release_artist in release_artists:
                release_artist_type = release_artist.get('artist_type_name', '')
                if release_artist_type == 'Artist':
                    components.append(release_artist.get('artist_name'))
        release_title = release_container.get('release_name')
        if release_title:
            components.append(release_title)
        return u' \u2013 '.join(components) if len(components) > 0 else None

    def get_release_info(self, release_container):
        release_label = release_container.get('label', {}).get('label_name')
        return u'Label: %s' % release_label if release_label else None

    def get_release_url(self, release_container):
        release_id = release_container.get('release_id')
        return 'https://www.beatport.com/release/-/%s' % release_id if release_id else None


class ScraperFactory(StandardFactory):
    scraper_classes = [ReleaseScraper]
    search_scraper = SearchScraper
