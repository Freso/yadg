#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import datetime
import json

import secret
from .base import Scraper, ExceptionMixin, RequestMixin, StandardFactory, GetReleaseResultMixin, GetListResultMixin
from ..result import NotFoundResult

READABLE_NAME = 'Bandcamp'
SCRAPER_URL = 'http://www.bandcamp.com/'
NOTES = u'YADG cannot search for releases on Bandcamp, you will always need to enter the exact link. Additionally, ' \
        u"links to Bandcamp artists or albums are only picked up automatically if the URL contains the string " \
        u"`/album/` or `.bandcamp.com`. In all other cases you need to force the use of the Bandcamp scraper by " \
        u"choosing it from the drop down menu (this will result in the input being interpreted as a Bandcamp URL)."


_API_KEY = secret.BANDCAMP_API_KEY


class ReleaseScraper(GetReleaseResultMixin, Scraper, ExceptionMixin, RequestMixin):

    base_url = 'http://api.bandcamp.com/api/'
    api_key = _API_KEY

    forced_response_encoding = 'utf8'

    string_regex = '^(http(?:s)?://\S+?/album/\S*)$'

    VARIOUS_ARTISTS_ALIASES = ['various artists']

    def __init__(self, album_url):
        super(ReleaseScraper, self).__init__()
        self.album_url = album_url

    def get_instance_info(self):
        return u'url=%s' % self.album_url

    def parse_response_content(self, response_content):
        try:
            response = json.loads(response_content)
        except:
            self.raise_exception(u'invalid server response: %r' % response_content)
        return response

    def add_release_event(self):
        if 'release_date' in self.data:
            release_event = self.result.create_release_event()
            date = datetime.datetime.fromtimestamp(self.data['release_date'])
            release_date = unicode(date.year)
            release_event.set_date(release_date)
            self.result.append_release_event(release_event)

    def add_release_format(self):
        if 'downloadable' in self.data:
            if self.data['downloadable'] == 1:
                format = 'Free WEB release'
            else:
                format = 'WEB release'
            self.result.set_format(format)

    def add_release_title(self):
        if 'title' in self.data:
            self.result.set_title(self.data['title'])

    def add_release_artists(self):
        artist_name = None
        if 'artist' in self.data:
            artist_name = self.data['artist']
        elif 'band_id' in self.data:
            response = self.request_get(url=self.base_url + 'band/3/info', params={'band_id': self.data['band_id'], 'key': self.api_key})
            band_info_response = self.parse_response_content(self.get_response_content(response))
            if 'error' in band_info_response and band_info_response['error']:
                self.raise_exception(u'could not get band info')
            elif 'name' in band_info_response:
                artist_name = band_info_response['name']
        if artist_name:
            artist = self.result.create_artist()
            if artist_name.lower() in self.VARIOUS_ARTISTS_ALIASES:
                artist.set_various(True)
            else:
                artist.set_name(artist_name)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)

    def get_disc_containers(self):
        if 'tracks' in self.data:
            return {1: self.data['tracks']}
        return {}

    def get_track_number(self, track_container):
        if 'number' in track_container:
            return str(track_container['number'])
        return None

    def get_track_title(self, track_container):
        if 'title' in track_container:
            return track_container['title']
        return None

    def get_track_length(self, track_container):
        if 'duration' in track_container:
            return int(round(track_container['duration']))
        return None

    def get_track_artists(self, track_container):
        artists = []
        if 'artist' in track_container:
            track_artist = self.result.create_artist()
            track_artist.set_name(track_container['artist'])
            track_artist.append_type(self.result.ArtistTypes.MAIN)
            artists.append(track_artist)
        return artists

    def initialize_data(self):
        # we don't care to catch any StatusCode exceptions here, if the status code of the api call is not equal to 200
        # then something is wrong with the api
        response = self.request_get(url=self.base_url + 'url/1/info', params={'url': self.album_url, 'key': self.api_key})

        # if we got no album ID, we cannot find the release
        album_discovery_response = self.parse_response_content(self.get_response_content(response))
        if not 'album_id' in album_discovery_response:
            self.result = self.instantiate_result(NotFoundResult)
        else:
            # again: we don't catch a status code error
            response = self.request_get(url=self.base_url + 'album/2/info', params={'album_id': album_discovery_response['album_id'], 'key': self.api_key})

            self.data = self.parse_response_content(self.get_response_content(response))

            # the api could not give us the album
            if 'error' in self.data and self.data['error']:
                self.result = self.instantiate_result(NotFoundResult)


class DiscographyScraper(GetListResultMixin, Scraper, ExceptionMixin, RequestMixin):

    base_url = 'http://api.bandcamp.com/api/'
    api_key = _API_KEY

    forced_response_encoding = 'utf8'

    string_regex = '^(http(?:s)?://\S+?\.bandcamp\.com(?:/\S*)?)$'

    def __init__(self, search_term):
        super(DiscographyScraper, self).__init__()
        self.band_url = search_term

    def get_instance_info(self):
        return u'band_url=%s' % self.band_url

    def parse_response_content(self, response_content):
        try:
            response = json.loads(response_content)
        except:
            self.raise_exception(u'invalid server response')
        return response

    def get_release_containers(self):
        if 'discography' in self.data:
            return self.data['discography']
        else:
            return []

    def get_release_name(self, release_container):
        components = []
        if 'artist' in release_container:
            components.append(release_container['artist'])
        if 'title' in release_container:
            components.append(release_container['title'])
        release_name = u' \u2013 '.join(components)
        if release_name:
            return release_name
        return None

    def get_release_url(self, release_container):
        if 'url' in release_container and '/album/' in release_container['url']:
            return release_container['url']
        return None

    def get_release_info(self, release_container):
        if 'release_date' in release_container:
            try:
                date = datetime.datetime.fromtimestamp(release_container['release_date'])
            except ValueError:
                date = None
            if date:
                date_string = date.strftime('%Y-%m-%d')
                return 'Release date: %s' % date_string
        return None

    def get_query_scraper(self, release_container):
        return self.get_name()

    def initialize_data(self):
        # we don't care to catch any StatusCode exceptions here, if the status code of the api call is not equal to 200
        # then something is wrong with the api
        response = self.request_get(url=self.base_url + 'url/1/info', params={'url': self.band_url, 'key': self.api_key})

        # if we got no band ID, we cannot find the discography
        band_discovery_response = self.parse_response_content(self.get_response_content(response))
        if not 'band_id' in band_discovery_response:
            self.result = self.instantiate_result(NotFoundResult)
        else:
            band_id = band_discovery_response['band_id']
            # again: we don't catch a status code error
            response = self.request_get(url=self.base_url + 'band/3/discography', params={'band_id': band_id, 'key': self.api_key})
            self.data = self.parse_response_content(self.get_response_content(response))


class ScraperFactory(StandardFactory):

    scraper_classes = [ReleaseScraper, DiscographyScraper]
    search_scraper = DiscographyScraper

    def is_searchable(self):
        return False